#!/bin/bash
echo "Build started on $(date)"
set -e
npm install
npm update

if [ -z "$CI_COMMIT_REF_NAME" ]; then
    GIT_BRANCH=$(git symbolic-ref --short -q HEAD)
else
    GIT_BRANCH=$CI_COMMIT_REF_NAME
fi

if [ "$GIT_BRANCH" = "master" ] 
then
    ##############Android build#################
    echo "Android Build started on $(date)"
    pushd .
    cd android/
    echo "---------- local.properties -----------"
    touch local.properties
    cat local.properties || true
    echo "---------- end local.properties -----------"
    gradle --refresh-dependencies \
           assembleRelease \
           uploadToHockeyApp
           
    ##############IOS build#################
    #echo "IOS Build started on $(date)"
    #pushd .
    #cd ios
    # ------------ pods update -----------
    #pod update
    # ------------ build and upload to hockeyapp using fastlane----------
    #fastlane beta HOCKEYAPP_TOKEN:$HOCKEYAPP_TOKEN
    #popd       
fi

#if [ $('uname') = 'Linux' ]
#then

#    popd
#elif [ $('uname') = 'Darwin' ]
#then

#else
#    echo "unknown platform"
#    exit 1
#fi
